from layers import GraphConvolution, GraphConvolutionSparse, InnerDecoder, Dense, IAF
import tensorflow as tf
from layers import DenseBlock, TInnerDecoder
import numpy as np

flags = tf.flags
FLAGS = flags.FLAGS


class Model(object):

    def __init__(self, **kwargs):
        allowed_kwargs = {'name', 'logging'}
        for kwarg in kwargs.keys():
            assert kwarg in allowed_kwargs, 'Invalid keyword argument: ' + kwarg

        for kwarg in kwargs.keys():
            assert kwarg in allowed_kwargs, 'Invalid keyword argument: ' + kwarg
        name = kwargs.get('name')
        if not name:
            name = self.__class__.__name__.lower()
        self.name = name

        logging = kwargs.get('logging', False)
        self.logging = logging

        self.vars = {}

    def _build(self):
        raise NotImplementedError

    def build(self):
        """ Wrapper for _build() """
        with tf.variable_scope(self.name):
            self._build()
        variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name)
        self.vars = {var.name: var for var in variables}

    def fit(self):
        pass

    def predict(self):
        pass


class CAN(Model):

    def __init__(self, placeholders, num_features, num_nodes, features_nonzero, **kwargs):
        super(CAN, self).__init__(**kwargs)

        self.inputs = placeholders['features']
        self.input_dim = num_features
        self.features_nonzero = features_nonzero
        self.n_samples = num_nodes
        self.adj = placeholders['adj']
        self.dropout = placeholders['dropout']
        self.build()

    def _build(self):
        self.hidden1 = GraphConvolutionSparse(input_dim=self.input_dim,
                                              output_dim=FLAGS.hidden1,
                                              adj=self.adj,
                                              features_nonzero=self.features_nonzero,
                                              act=tf.nn.relu,
                                              dropout=self.dropout,
                                              logging=self.logging)(self.inputs)

        self.hidden2 = Dense(input_dim=self.n_samples,
                                              output_dim=FLAGS.hidden1,
                                              act=tf.nn.tanh,
                                              sparse_inputs=True,
                                              dropout=self.dropout)(tf.sparse_transpose(self.inputs))
        self.z_u_mean = GraphConvolution(input_dim=FLAGS.hidden1,
                                       output_dim=FLAGS.hidden2,
                                       adj=self.adj,
                                       act=lambda x: x,
                                       dropout=self.dropout,
                                       logging=self.logging)(self.hidden1)

        self.z_u_log_std = GraphConvolution(input_dim=FLAGS.hidden1,
                                          output_dim=FLAGS.hidden2,
                                          adj=self.adj,
                                          act=lambda x: x,
                                          dropout=self.dropout,
                                          logging=self.logging)(self.hidden1)
        self.z_a_mean = Dense(input_dim=FLAGS.hidden1,
                                       output_dim=FLAGS.hidden2,
                                       act=lambda x: x,
                                       dropout=self.dropout)(self.hidden2)

        self.z_a_log_std = Dense(input_dim=FLAGS.hidden1,
                                          output_dim=FLAGS.hidden2,
                                          act=lambda x: x,
                                          dropout=self.dropout)(self.hidden2)

        self.z_u = self.z_u_mean + tf.random_normal([self.n_samples, FLAGS.hidden2], dtype=tf.float64) * tf.exp(self.z_u_log_std)
        self.z_a = self.z_a_mean + tf.random_normal([self.input_dim, FLAGS.hidden2], dtype=tf.float64) * tf.exp(self.z_a_log_std)

        self.reconstructions = InnerDecoder(input_dim=FLAGS.hidden2,
                                      act=lambda x: x,
                                      logging=self.logging)((self.z_u, self.z_a))


class IAF_CAN1(Model):
    def __init__(self, placeholders, num_features, num_nodes, features_nonzero, **kwargs):
        super(IAF_CAN1, self).__init__(**kwargs)

        self.inputs = placeholders['features']
        self.input_dim = num_features
        self.features_nonzero = features_nonzero
        self.n_samples = num_nodes
        self.adj = placeholders['adj']
        self.adj_orig = placeholders['adj_orig']
        self.dropout = placeholders['dropout']
        self.build()

    def encode(self):
        self.hidden1 = GraphConvolutionSparse(input_dim=self.input_dim,
                                              output_dim=FLAGS.hidden1,
                                              adj=self.adj,
                                              features_nonzero=self.features_nonzero,
                                              act=tf.nn.relu,
                                              dropout=self.dropout,
                                              logging=self.logging)(self.inputs)

        self.hidden2 = Dense(input_dim=self.n_samples,
                             output_dim=FLAGS.hidden1,
                             act=tf.nn.tanh,
                             sparse_inputs=True,
                             dropout=self.dropout)(tf.sparse_transpose(self.inputs))
        self.z_u_mean = GraphConvolution(input_dim=FLAGS.hidden1,
                                         output_dim=FLAGS.hidden2,
                                         adj=self.adj,
                                         act=lambda x: x,
                                         dropout=self.dropout,
                                         logging=self.logging)(self.hidden1)

        self.z_u_log_std = GraphConvolution(input_dim=FLAGS.hidden1,
                                            output_dim=FLAGS.hidden2,
                                            adj=self.adj,
                                            act=lambda x: x,
                                            dropout=self.dropout,
                                            logging=self.logging)(self.hidden1)

        self.z_u_h = GraphConvolution(input_dim=FLAGS.hidden1,
                                      output_dim=FLAGS.h_size,
                                      adj=self.adj,
                                      act=lambda x: x,
                                      dropout=self.dropout,
                                      logging=self.logging)(self.hidden1)

        self.z_a_mean = Dense(input_dim=FLAGS.hidden1,
                              output_dim=FLAGS.hidden2,
                              act=lambda x: x,
                              dropout=self.dropout)(self.hidden2)

        self.z_a_log_std = Dense(input_dim=FLAGS.hidden1,
                                 output_dim=FLAGS.hidden2,
                                 act=lambda x: x,
                                 dropout=self.dropout)(self.hidden2)

        self.z_a_h = Dense(input_dim=FLAGS.hidden1,
                           output_dim=FLAGS.h_size,
                           act=lambda x: x,
                           dropout=self.dropout)(self.hidden2)

    def _build(self):
        self.encode()

        self.eps_u = tf.random_normal([self.n_samples, FLAGS.hidden2], dtype=tf.float64)
        self.eps_a = tf.random_normal([self.input_dim, FLAGS.hidden2], dtype=tf.float64)

        self.z_u_0 = self.z_u_mean + self.eps_u * tf.exp(self.z_u_log_std)
        self.z_a_0 = self.z_a_mean + self.eps_a * tf.exp(self.z_a_log_std)
        # TODO
        self.node_flow = IAF(z_size=FLAGS.hidden2, num_flows=FLAGS.num_flows,
                              num_hidden=1, h_size=FLAGS.h_size)
        self.z_u_k, self.u_log_det_j = self.node_flow((self.z_u_0, self.z_u_h))
        # TODO
        self.attr_flow = IAF(z_size=FLAGS.hidden2, num_flows=FLAGS.num_flows,
                              num_hidden=1, h_size=FLAGS.h_size)
        self.z_a_k, self.a_log_det_j = self.attr_flow((self.z_a_0, self.z_a_h))

        self.reconstructions, self.weights_a = TInnerDecoder(input_dim=FLAGS.hidden2,
                                            act=lambda x: x,
                                            logging=self.logging)((self.z_u_k, self.z_a_k))

    def vp_change(self, sn = 1000):
        # only consider the first element
        z_u_mean = tf.slice(self.z_u_mean, [0, 0], [1, FLAGS.hidden2])  # 1*D
        z_a_mean = tf.slice(self.z_a_mean, [0, 0], [1, FLAGS.hidden2])  # 1*D
        z_u_h = tf.slice(self.z_u_h, [0, 0], [1, FLAGS.h_size])
        z_u_log_std = tf.slice(self.z_u_log_std, [0, 0], [1, FLAGS.hidden2])  # 1*D
        z_a_log_std = tf.slice(self.z_a_log_std, [0, 0], [1, FLAGS.hidden2])  # 1*D
        z_a_h = tf.slice(self.z_a_h, [0, 0], [1, FLAGS.h_size])

        flag = True

        ran_u_min = z_u_mean - 3 * tf.exp(z_u_log_std)
        ran_u_max = z_u_mean + 3 * tf.exp(z_u_log_std)
        ran_a_min = z_a_mean - 3 * tf.exp(z_a_log_std)
        ran_a_max = z_a_mean + 3 * tf.exp(z_a_log_std)

        for i in range(sn):
            z_u_0 = ran_u_min + (ran_u_max - ran_u_min) * tf.random_uniform([1, FLAGS.hidden2], dtype=tf.float64)
            z_a_0 = ran_a_min + (ran_a_max - ran_a_min) * tf.random_uniform([1, FLAGS.hidden2], dtype=tf.float64)

            # z_u_0 = z_u_mean + eps_u * tf.exp(z_u_log_std)  # 1*D
            # z_a_0 = z_a_mean + eps_a * tf.exp(z_a_log_std)  # 1*D
            eps_u = (z_u_0 - z_u_mean) / tf.exp(z_u_log_std)
            eps_a = (z_a_0 - z_a_mean) / tf.exp(z_a_log_std)

            # used for experiment: how posterior change?
            logqz0_u = -0.5 * FLAGS.hidden2 * tf.log(2 * np.float64(np.pi)) - tf.reduce_sum(z_u_log_std) - 0.5 * tf.reduce_sum(tf.square(eps_u))
            logqz0_a = -0.5 * FLAGS.hidden2 * tf.log(2 * np.float64(np.pi)) - tf.reduce_sum(z_a_log_std) - 0.5 * tf.reduce_sum(tf.square(eps_a))
            z_u_k, u_log_det_j = self.node_flow((z_u_0, z_u_h))
            z_a_k, a_log_det_j = self.attr_flow((z_a_0, z_a_h))

            # now combine them
            if flag:
                self.z_u_0s = z_u_0
                self.z_a_0s = z_a_0
                self.logqz0_us = tf.expand_dims(tf.expand_dims(logqz0_u, 0), 0)
                self.logqz0_as = tf.expand_dims(tf.expand_dims(logqz0_a, 0), 0)
                self.z_u_ks = z_u_k
                self.logqzt_us = tf.expand_dims(logqz0_u - u_log_det_j, 0)
                self.z_a_ks = z_a_k
                self.logqzt_as = tf.expand_dims(logqz0_a - a_log_det_j, 0)
                flag = False
            else:
                self.z_u_0s = tf.concat([self.z_u_0s, z_u_0], 0)
                self.z_a_0s = tf.concat([self.z_a_0s, z_a_0], 0)
                self.logqz0_us = tf.concat([self.logqz0_us, tf.expand_dims(tf.expand_dims(logqz0_u, 0), 0)], 0)
                self.logqz0_as = tf.concat([self.logqz0_as, tf.expand_dims(tf.expand_dims(logqz0_a, 0), 0)], 0)
                self.z_u_ks = tf.concat([self.z_u_ks, z_u_k], 0)
                self.logqzt_us = tf.concat([self.logqzt_us, tf.expand_dims(logqz0_u - u_log_det_j, 0)], 0)
                self.z_a_ks = tf.concat([self.z_a_ks, z_a_k], 0)
                self.logqzt_as = tf.concat([self.logqzt_as, tf.expand_dims(logqz0_a - a_log_det_j, 0)], 0)
