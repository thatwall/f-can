## A Normalizing Flow-based Co-embedding Model for Attributed Networks
This repository contains the Python implementation for F-CAN. Further details about F-CAN can be found in our paper:
> Zhuo Ouyang, Shangsong Liang, and Zaiqiao Meng. A Normalizing Flow-based Co-embedding Model for Attributed Networks. (IEEE Transactions on Neural Networks and Learning Systems)


## Requirements
=================
* TensorFlow (1.0 or later)
* python 2.7/3.6
* scikit-learn
* scipy

## Run the demo
=================
```bash
python train.py
```
