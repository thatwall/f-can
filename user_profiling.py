import numpy as np
import csv

def user_profiling():
    nodeidx_file = open('data/DBLP.nodidx', 'r')
    attridx_file = open('data/DBLP.attidx', 'r')
    nodeidx = nodeidx_file.readlines()
    attridx = attridx_file.readlines()
    nodenum = int(nodeidx[0].split('\t')[1].strip())
    attrnum = int(attridx[0].split('\t')[1].strip())
    cosSimilarity = np.load('result/AGAE_DBLP_cs.npy')[np.newaxis, :]
    # np.reshape(cosSimilarity, (nodenum, attrnum))
    cosSimilarity = cosSimilarity.reshape((nodenum, -1))
    cosnlarge = []  # each row is the top-4 attr-idxs
    for i in range(cosSimilarity.shape[0]):
        sortidx = sorted(enumerate(cosSimilarity[i]), key=lambda x: x[1])
        sortidx = sortidx[-10:]
        sortidx.reverse()
        cosnlarge.append(sortidx)
    assert cosSimilarity.shape[0] == nodenum and cosSimilarity.shape[1] == attrnum
    print("node_num:{},attribute_num:{}".format(nodenum,attrnum))
    nodeidx.pop(0)
    attridx.pop(0)
    nodes = []
    attrs = []
    for i in range(len(nodeidx)):
        nodes.append(nodeidx[i].split('\t')[1])
    for i in range(len(attridx)):
        attrs.append(attridx[i].split('\t')[1])

    out = open('user_profiling.csv', 'a', newline='')
    csv_write = csv.writer(out, dialect='excel')
    for i, name in enumerate(nodes):
        nodes[i] = [nodes[i]]
        for j in cosnlarge[i]:
            nodes[i].append(attrs[j[0]])
            nodes[i].append(j[1])
        csv_write.writerow(nodes[i])
    print('finish')

if __name__ == '__main__':
    user_profiling()