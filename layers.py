import tensorflow as tf
import numpy as np
from sklearn.manifold import t_sne

flags = tf.flags
FLAGS = flags.FLAGS
latent_dim = 128
hidden_decoder_dim = 512

# global unique layer ID dictionary for layer name assignment
_LAYER_UIDS = {}


def get_layer_uid(layer_name=''):
    """Helper function, assigns unique layer IDs
    """
    if layer_name not in _LAYER_UIDS:
        _LAYER_UIDS[layer_name] = 1
        return 1
    else:
        _LAYER_UIDS[layer_name] += 1
        return _LAYER_UIDS[layer_name]


def dropout_sparse(x, keep_prob, num_nonzero_elems):
    """Dropout for sparse tensors. Currently fails for very large sparse tensors (>1M elements)
    """
    noise_shape = [num_nonzero_elems]
    random_tensor = keep_prob
    random_tensor += tf.random_uniform(noise_shape, dtype=tf.float64)
    dropout_mask = tf.cast(tf.floor(random_tensor), dtype=tf.bool)
    pre_out = tf.sparse_retain(x, dropout_mask)
    return pre_out * (1. / keep_prob)


class Layer(object):
    """Base layer class. Defines basic API for all layer objects.

    # Properties
        name: String, defines the variable scope of the layer.

    # Methods
        _call(inputs): Defines computation graph of layer
            (i.e. takes input, returns output)
        __call__(inputs): Wrapper for _call()
    """

    def __init__(self, **kwargs):
        allowed_kwargs = {'name', 'logging'}
        for kwarg in kwargs.keys():
            assert kwarg in allowed_kwargs, 'Invalid keyword argument: ' + kwarg
        name = kwargs.get('name')
        if not name:
            layer = self.__class__.__name__.lower()
            name = layer + '_' + str(get_layer_uid(layer))
        self.name = name
        self.vars = {}
        logging = kwargs.get('logging', False)
        self.logging = logging
        self.issparse = False

    def _call(self, inputs):
        return inputs

    def __call__(self, inputs):
        with tf.name_scope(self.name):
            outputs = self._call(inputs)
            return outputs

class MaskedLinear(Layer):
    def __init__(self, in_dim, out_dim, isact = True, act = tf.nn.elu, diag_zero = False, bias = True, **kwargs):
        super(MaskedLinear, self).__init__(**kwargs)
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.diag_zero = diag_zero
        self.isact = isact
        self.act = act
        with tf.variable_scope(self.name):
            self.vars['weights'] = tf.get_variable('w', shape=(in_dim, out_dim), dtype=tf.float64,
                                                   initializer=tf.contrib.layers.xavier_initializer(),
                                                   regularizer=tf.contrib.layers.l2_regularizer(FLAGS.weight_decay))
            if bias:
                self.vars['bias'] = tf.get_variable('b', shape=(out_dim), dtype=tf.float64,
                                                    initializer=tf.zeros_initializer(),)
            else:
                self.vars['bias'] = None

        mask = self.build_mask()
        with tf.variable_scope(self.name):
            self.mask = tf.convert_to_tensor(mask)

    def build_mask(self):
        n_in, n_out = self.in_dim, self.out_dim
        assert n_in % n_out == 0 or n_out % n_in == 0

        mask = np.ones((n_in, n_out), dtype=np.float64)
        if n_out >= n_in:
            k = n_out // n_in
            for i in range(n_in):
                mask[i + 1:, i * k:(i + 1) * k] = 0
                if self.diag_zero:
                    mask[i:i + 1, i * k:(i + 1) * k] = 0
        else:
            k = n_in // n_out
            for i in range(n_out):
                mask[(i + 1) * k:, i:i + 1] = 0
                if self.diag_zero:
                    mask[i * k:(i + 1) * k:, i:i + 1] = 0
        return mask

    def _call(self, inputs):
        outputs = tf.matmul(inputs, self.mask * self.vars['weights'])
        if self.vars['bias'] is not None:
            outputs += self.vars['bias']
        if self.isact:
            return self.act(outputs)
        else:
            return outputs

class DenseBlock(Layer):
    def __init__(self, in_dim, out_dim, adj, features_nonzero, dropout=0., act=tf.nn.relu, **kwargs):
        super(DenseBlock, self).__init__(**kwargs)
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.adj = adj
        self.features_nonzero = features_nonzero
        self.dropout = dropout
        self.act = act
        with tf.variable_scope(self.name):
            self.vars['weights'] = tf.get_variable('w', shape=(in_dim, out_dim), dtype=tf.float64,
                                                   initializer=tf.contrib.layers.xavier_initializer(),
                                                   regularizer=tf.contrib.layers.l2_regularizer(FLAGS.weight_decay))

    def _call(self, inputs):
        x = inputs
        x = dropout_sparse(x, 1 - self.dropout, self.features_nonzero)
        adj = tf.sparse_tensor_dense_matmul(self.adj, self.vars['weights'])
        x = tf.sparse_tensor_dense_matmul(x, adj)
        outputs = self.act(x)
        return outputs

class IAF(Layer):
    def __init__(self, z_size, num_flows=2, num_hidden=0, h_size=50, forget_bias=1., **kwargs):
        super(IAF, self).__init__(**kwargs)
        self.z_size = z_size
        self.num_flows = num_flows
        self.num_hidden = num_hidden
        self.h_size = h_size
        self.forget_bias = forget_bias

        ar_layer = MaskedLinear
        self.act = tf.nn.elu
        self.flows = []
        # self.flip_idx =

        for k in range(num_flows):
            z_feats = ar_layer(z_size, h_size)
            zh_feats = []
            for j in range(num_hidden):
                zh_feats += [ar_layer(h_size, h_size)]
            linear_mean = ar_layer(h_size, z_size, False, diag_zero=True)
            linear_std = ar_layer(h_size, z_size, False, diag_zero=True)
            self.flows.append((z_feats, zh_feats, linear_mean, linear_std))


    def _call(self, inputs):
        z, h_context = inputs  # (batch, z_size)
        logdets = 0.
        for i, flow in enumerate(self.flows):
            if (i + 1) % 2 == 0:
                z = tf.reverse(z, axis=[1])
            h = flow[0](z)
            h = h + h_context
            for j in range(self.num_hidden):
                h = flow[1][j](h)
            mean = flow[2](h)
            gate = tf.nn.sigmoid(flow[3](h) + self.forget_bias)
            # gate = tf.clip_by_value(gate, 1e-300, 1)  # TODO: modified
            z = gate * z + (1 - gate) * mean
            logdets += tf.reduce_sum(tf.log(gate), 1)  # TODO: modified

            # logdets += tf.log(gate)  # TODO: modified
        return z, logdets# TODO: modified



class GraphConvolution(Layer):
    """Basic graph convolution layer for undirected graph without edge labels."""

    def __init__(self, input_dim, output_dim, adj, dropout=0., act=tf.nn.relu, **kwargs):
        super(GraphConvolution, self).__init__(**kwargs)
        with tf.variable_scope(self.name + '_vars'):
            self.vars['weights'] = weight_variable_glorot(input_dim, output_dim, name="weights")
        self.dropout = dropout
        self.adj = adj
        self.act = act

    def _call(self, inputs):
        x = inputs
        x = tf.nn.dropout(x, 1 - self.dropout)
        x = tf.matmul(x, self.vars['weights'])
        x = tf.sparse_tensor_dense_matmul(self.adj, x)
        outputs = self.act(x)
        return outputs


class GraphConvolutionSparse(Layer):
    """Graph convolution layer for sparse inputs."""

    def __init__(self, input_dim, output_dim, adj, features_nonzero, dropout=0., act=tf.nn.relu, **kwargs):
        super(GraphConvolutionSparse, self).__init__(**kwargs)
        with tf.variable_scope(self.name + '_vars'):
            self.vars['weights'] = weight_variable_glorot(input_dim, output_dim, name="weights")
        self.dropout = dropout
        self.adj = adj
        self.act = act
        self.issparse = True
        self.features_nonzero = features_nonzero

    def _call(self, inputs):
        x = inputs
        x = dropout_sparse(x, 1 - self.dropout, self.features_nonzero)
        x = tf.sparse_tensor_dense_matmul(x, self.vars['weights'])
        x = tf.sparse_tensor_dense_matmul(self.adj, x)
        outputs = self.act(x)
        return outputs


class Dense(Layer):
    """Dense layer."""

    def __init__(self, input_dim, output_dim, dropout=0.,
                 act=tf.nn.relu, placeholders=None, bias=True,
                 sparse_inputs=False, **kwargs):
        super(Dense, self).__init__(**kwargs)
        self.dropout = dropout
        self.act = act
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.bias = bias
        # helper variable for sparse dropout
        self.sparse_inputs = sparse_inputs

        with tf.variable_scope(self.name + '_vars'):
            self.vars['weights'] = tf.get_variable('weights', shape=(input_dim, output_dim),
                                         dtype=tf.float64,
                                         initializer=tf.contrib.layers.xavier_initializer(),
                                         regularizer=tf.contrib.layers.l2_regularizer(FLAGS.weight_decay))
            if self.bias:
                self.vars['bias'] = tf.Variable(tf.zeros([output_dim], dtype=tf.float64), name='bias')

        if self.logging:
            self._log_vars()

    def _call(self, inputs):
        x = inputs

        if self.sparse_inputs:
            output = tf.sparse_tensor_dense_matmul(x, self.vars['weights'])
        else:
            output = tf.matmul(x, self.vars['weights'])

        # bias
        if self.bias:
            output += self.vars['bias']

        return self.act(output)


class InnerProductDecoder(Layer):
    """Decoder model layer for link prediction."""

    def __init__(self, input_dim, dropout=0., act=tf.nn.sigmoid, **kwargs):
        super(InnerProductDecoder, self).__init__(**kwargs)
        self.dropout = dropout
        self.act = act

    def _call(self, inputs):
        inputs = tf.nn.dropout(inputs, 1 - self.dropout)
        print("inputs.shape:", inputs)
        x = tf.transpose(inputs)
        x = tf.matmul(inputs, x)
        print("x = tf.matmul(inputs, x):", x)

        x = tf.reshape(x, [-1])
        outputs = self.act(x)
        return outputs


def weight_variable_glorot(input_dim, output_dim, name=""):
    """Create a weight variable with Glorot & Bengio (AISTATS 2010)
    initialization.
    """
    init_range = np.sqrt(6.0 / (input_dim + output_dim))
    initial = tf.random_uniform([input_dim, output_dim], minval=-init_range,
                                maxval=init_range, dtype=tf.float64)
    return tf.Variable(initial, name=name)


def bais_variable_glorot(output_dim, name=""):
    """Create a weight variable with Glorot & Bengio (AISTATS 2010)
    initialization.
    """
    init_range = np.sqrt(6.0 / (output_dim))
    initial = tf.random_uniform([output_dim], minval=-init_range,
                                maxval=init_range, dtype=tf.float64)
    return tf.Variable(initial, name=name)


class InnerDecoder(Layer):
    """Decoder model layer for link prediction."""

    def __init__(self, input_dim, dropout=0., act=tf.nn.sigmoid, **kwargs):
        super(InnerDecoder, self).__init__(**kwargs)
        self.dropout = dropout
        self.input_dim = input_dim
        self.act = act

    def _call(self, inputs):
        z_u, z_a = inputs
        z_u = tf.nn.dropout(z_u, 1 - self.dropout)
        z_u_t = tf.transpose(z_u)
        x = tf.matmul(z_u, z_u_t)
        # x = tf.reshape(x, [-1])
        z_a_t = tf.transpose(tf.nn.dropout(z_a, 1 - self.dropout))
        y = tf.matmul(z_u, z_a_t)
        edge_outputs = tf.reshape(self.act(x), [-1])
        attri_outputs = tf.reshape(self.act(y), [-1])
        return edge_outputs, attri_outputs

class TInnerDecoder(Layer):
    """Decoder model layer for link prediction."""

    def __init__(self, input_dim, dropout=0., act=tf.nn.sigmoid, **kwargs):
        super(TInnerDecoder, self).__init__(**kwargs)
        self.dropout = dropout
        self.input_dim = input_dim
        self.act = act
        with tf.variable_scope(self.name):
            self.vars['weights_u'] = tf.get_variable('w_u', shape=(input_dim, input_dim),
                                         dtype=tf.float64,
                                         initializer=tf.contrib.layers.xavier_initializer(),
                                         regularizer=tf.contrib.layers.l2_regularizer(FLAGS.weight_decay))
            self.vars['weights_a'] = tf.get_variable('w_a', shape=(input_dim, input_dim),
                                         dtype=tf.float64,
                                         initializer=tf.contrib.layers.xavier_initializer(),
                                         regularizer=tf.contrib.layers.l2_regularizer(FLAGS.weight_decay))

    def _call(self, inputs):
        z_u, z_a = inputs
        z_u = tf.nn.dropout(z_u, 1 - self.dropout)
        z_u_t = tf.transpose(z_u)
        x = tf.matmul(z_u, tf.matmul(self.vars['weights_u'], z_u_t))
        # x = tf.reshape(x, [-1])
        z_a_t = tf.transpose(tf.nn.dropout(z_a, 1 - self.dropout))
        y = tf.matmul(z_u, tf.matmul(self.vars['weights_a'], z_a_t))

        edge_outputs = tf.reshape(self.act(x), [-1])
        attri_outputs = tf.reshape(self.act(y), [-1])
        return (edge_outputs, attri_outputs), self.vars['weights_a']
