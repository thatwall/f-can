# this file is used for creating appropriate form for those code who creates heat-map
import numpy as np

dataset_str = 'cora'
embedding_node_mean0_result_file = "../result/AGAE_{}_n_mu0.emb.npy".format(dataset_str)
embedding_attr_mean0_result_file = "../result/AGAE_{}_a_mu0.emb.npy".format(dataset_str)
embedding_node_pd0_result_file = "../result/AGAE_{}_n_pd0.emb.npy".format(dataset_str)
embedding_attr_pd0_result_file = "../result/AGAE_{}_a_pd0.emb.npy".format(dataset_str)
embedding_node_mean_result_file = "../result/AGAE_{}_n_mut.emb.npy".format(dataset_str)
embedding_attr_mean_result_file = "../result/AGAE_{}_a_mut.emb.npy".format(dataset_str)
embedding_node_pdt_result_file = "../result/AGAE_{}_n_pdt.emb.npy".format(dataset_str)
embedding_attr_pdt_result_file = "../result/AGAE_{}_a_pdt.emb.npy".format(dataset_str)

embedding_node_mean0 = np.load(embedding_node_mean0_result_file)  # N*2
embedding_attr_mean0 = np.load(embedding_attr_mean0_result_file)  # F*2
embedding_node_mean0 = np.hsplit(embedding_node_mean0, 2)
embedding_attr_mean0 = np.hsplit(embedding_attr_mean0, 2)
embedding_node_mean0_x = embedding_node_mean0[0]
embedding_node_mean0_y = embedding_node_mean0[1]
embedding_attr_mean0_x = embedding_attr_mean0[0]
embedding_attr_mean0_y = embedding_attr_mean0[1]
embedding_node_pd0 = np.load(embedding_node_pd0_result_file)  # N*1
embedding_attr_pd0 = np.load(embedding_attr_pd0_result_file)  # F*1

embedding_node_mean = np.load(embedding_node_mean_result_file)
embedding_attr_mean = np.load(embedding_attr_mean_result_file)
embedding_node_mean = np.hsplit(embedding_node_mean, 2)
embedding_attr_mean = np.hsplit(embedding_attr_mean, 2)
embedding_node_mean_x = embedding_node_mean[0]
embedding_node_mean_y = embedding_node_mean[1]
embedding_attr_mean_x = embedding_attr_mean[0]
embedding_attr_mean_y = embedding_attr_mean[1]
embedding_node_pdt = np.load(embedding_node_pdt_result_file)
embedding_attr_pdt = np.load(embedding_attr_pdt_result_file)

np.savetxt('../result/cora/AGAE_{}_n_mu0_x'.format(dataset_str), embedding_node_mean0_x)
np.savetxt('../result/cora/AGAE_{}_n_mu0_y'.format(dataset_str), embedding_node_mean0_y)
np.savetxt('../result/cora/AGAE_{}_n_mu0_z'.format(dataset_str), embedding_node_pd0)

np.savetxt('../result/cora/AGAE_{}_a_mu0_x'.format(dataset_str), embedding_attr_mean0_x)
np.savetxt('../result/cora/AGAE_{}_a_mu0_y'.format(dataset_str), embedding_attr_mean0_y)
np.savetxt('../result/cora/AGAE_{}_a_mu0_z'.format(dataset_str), embedding_attr_pd0)

np.savetxt('../result/cora/AGAE_{}_n_mu_x'.format(dataset_str), embedding_node_mean_x)
np.savetxt('../result/cora/AGAE_{}_n_mu_y'.format(dataset_str), embedding_node_mean_y)
np.savetxt('../result/cora/AGAE_{}_n_mu_z'.format(dataset_str), embedding_node_pdt)

np.savetxt('../result/cora/AGAE_{}_a_mu_x'.format(dataset_str), embedding_attr_mean_x)
np.savetxt('../result/cora/AGAE_{}_a_mu_y'.format(dataset_str), embedding_attr_mean_y)
np.savetxt('../result/cora/AGAE_{}_a_mu_z'.format(dataset_str), embedding_attr_pdt)