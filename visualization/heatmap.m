%----------------------------
clear
clear all
% x0_str = '../result/cora/AGAE_cora_n_mu_x';
% y0_str = '../result/cora/AGAE_cora_n_mu_y';
% z0_str = '../result/cora/AGAE_cora_n_mu_z';
x_str = '../result/cora-f-can/AGAE_cora_n_mu_x';
y_str = '../result/cora-f-can/AGAE_cora_n_mu_y';
z_str = '../result/cora-f-can/AGAE_cora_n_mu_z';

% fidx0=fopen(x0_str,'r');
% x0=fscanf(fidx0,'%lf');
% %载入文件获取y的�??
% fidy0=fopen(y0_str,'r');
% y0=fscanf(fidy0,'%lf');
% %载入文件，获取z的�??
% fidz0=fopen(z0_str,'r');
% z0=fscanf(fidz0,'%lf');
% z0=exp(z0);

fidx=fopen(x_str,'r');
x=fscanf(fidx,'%lf');
%载入文件获取y的�??
fidy=fopen(y_str,'r');
y=fscanf(fidy,'%lf');
%载入文件，获取z的�??
fidz=fopen(z_str,'r');
z=fscanf(fidz,'%lf');
z=exp(z);
% x=[x0;x];
% y=[y0;y];
% z=[z0;z];
% zmean = mean(z);
% zstd = std(z);
% zlow = zmean-3*zstd;
% zhigh = zmean+3*zstd;
% rmidx1 = any(z>zhigh,2);
% rmidx2 = any(z<zlow,2);
% x(rmidx1,:) = [];
% y(rmidx1,:) = [];
% z(rmidx1,:) = [];
% x(rmidx2,:) = [];
% y(rmidx2,:) = [];
% z(rmidx2,:) = [];
%取x的最大�??
% maxx0 = max(x0);
% minx0 = min(x0);
% maxy0 = max(y0);
% miny0 = min(y0);
% minz0 = min(z0);
% maxz0 = max(z0);
% 
maxx=max(x);
%取x的最小�??
minx=min(x);
%同x
maxy=max(y);
miny=min(y);
maxz = max(z);
minz = min(z);
%生成网格
% [X0,Y0]=meshgrid(linspace(minx0,maxx0,500),linspace(miny0,maxy0,500));
[X,Y]=meshgrid(linspace(minx,maxx,1000),linspace(miny,maxy,1000));
%插入人员密度�?
% Z0=griddata(x0,y0,z0,X0,Y0,'v4');
Z=griddata(x,y,z,X,Y,'v4');
for i = 1:1000
    for j = 1:1000
        if(Z(i,j)<0)
            Z(i,j)=0;
        end
    end
end
% subplot(1,2,1);
% %生成三维�?
set(0,'DefaultAxesFontSize',20);
mesh(X,Y,Z)
axis square
% contour3(X,Y,Z)
hold on
% plot3(x,y,z,'c.')
% plot3(x(1:1000),y(1:1000),z(1:1000),'c.')
% plot3(x(1000:2000),y(1000:2000),z(1000:2000),'r.')
% hold on
%坐标命名
xlabel('X','Fontsize',7);
ylabel('Y','FontSize',7);
% zlabel('Z');
%插入颜色�?
colorbar
%二维视角
% subplot(1,2,2);
%生成三维�?
% mesh(X,Y,Z)
% contour3(X,Y,Z)
% hold on
%在三维面上画出人员密度�?�，高低峰岁值的大小而改变，颜色也是
% plot3(x(1:1000),y(1:1000),z(1:1000),'c.')
% plot3(x(1000:2000),y(1000:2000),z(1000:2000),'r.')
% plot3(x,y,z,'c.')
% hold on
% view(2);
% %坐标命名
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
% %插入颜色�?
% colorbar
%--------------------- 
%作�?�：J_Anson 
%来源：CSDN 
%原文：https://blog.csdn.net/j_anson/article/details/52751097 
%版权声明：本文为博主原创文章，转载请附上博文链接�?